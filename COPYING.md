# Copying notice

Flossy Gnu  
<https://source.puri.sm/Purism/flossy-gnu>  
Copyright 2018 Purism SPC  
SPDX-License-Identifier: GPL-3.0-or-later  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Notices for dependencies

All code is covered by and compatible with the "Copying notice" above.
Dependencies may have their own notices. These can be found in the
source code of those dependencies.

## Notices for included code

All code is covered by and compatible with the "Copying notice" above.
Some code copied from other sources comes with additional notices. These
are listed below, and also appear in the files where that code appears.

---

Community Covenant  
https://community-covenant.net  
Copyright 2016-2017 Coraline Ada Ehmke  
SPDX-License-Identifier: CC-BY-4.0  

Community Covenant by Coraline Ada Ehmke is licensed under a Creative
Commons Attribution 4.0 International License <http://creativecommons.org/licenses/by/4.0/>. Based on a work at <https://community-covenant.net>

---

MooGNU (a copyleft alternative to copyrighted NyanCat)  
https://archive.org/details/M00GNU  
Copyright 2012 /g/  
SPDX-License-Identifier: CC-BY-3.0  

This work was collaboratively made by anonymous posters on the 4chan technology image board /g/ and licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <https://creativecommons.org/licenses/by-sa/3.0/>

extends Label

onready var tween = $Tween

const WHITE_OPAQUE = Color('aaffffff')
const WHITE_SEMI_TRANSPARENT = Color('44ffffff')
const WHITE_TRANSPARENT = Color('00ffffff')

var score = 0 setget set_score


func setup(pillar_spawner):
	pillar_spawner.connect("pillar_checkpoint_reached", self, "_on_PillarSpawner_pillar_checkpoint_reached")


func set_score(value):
	score = value
	text = str(score)
	tween.interpolate_property(
		self, 'modulate',
		WHITE_OPAQUE, WHITE_SEMI_TRANSPARENT, 0.8,
		Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()


func fade_out():
	tween.stop_all()
	tween.interpolate_property(
		self, 'modulate',
		modulate, WHITE_TRANSPARENT, 0.6,
		Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()


func _ready():
	modulate = WHITE_SEMI_TRANSPARENT


func _on_PillarSpawner_pillar_checkpoint_reached():
	self.score += 1

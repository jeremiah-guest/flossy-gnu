# SPDX-License-Identifier: GPL-3.0-or-later
extends Node2D

onready var pillar_spawner = $Foreground/PillarSpawner
onready var score_label = $Background/ScoreLabel
onready var foreground = $Foreground
onready var gnu = $Gnu
onready var score_board = $UI/ScoreBoard

const ScoreLoader = preload("res://ScoreBoard/ScoreLoader.gd")

var waiting_to_restart = false


func _ready():
	score_label.setup(pillar_spawner)
	gnu.connect("died", self, "_on_Gnu_died")


func _unhandled_input(event):
	if waiting_to_restart:
		if event.is_action_pressed("ui_accept") or event.is_action_pressed("tap"):
			get_tree().reload_current_scene()
	if event.is_action_pressed("reset") and OS.is_debug_build():
		get_tree().reload_current_scene()


func _on_Gnu_died():
	"""Play fade out animations, update the score, and show the scoreboard"""
	foreground.fade_out()
	yield(get_tree().create_timer(0.3), "timeout")
	# Can't use an AnimationPlayer because the pillars are toplevel nodes
	get_tree().call_group('pillar', 'fade_out')
	yield(get_tree().create_timer(0.3), "timeout")
	score_label.fade_out()
	yield(get_tree().create_timer(0.6), "timeout")

	var loader = ScoreLoader.new()
	loader.register(score_label.score)
	var scores = loader.get_scores()
	score_board.start(scores)
	waiting_to_restart = true

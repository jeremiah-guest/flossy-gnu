extends Camera2D

onready var start_offset = position

func _ready():
	set_as_toplevel(true)
	position = owner.position + start_offset

func _physics_process(delta):
	position.x = owner.position.x + start_offset.x

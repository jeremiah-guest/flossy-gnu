"""
Saves and loads scores to/from the disk
Each score is a dictionary of the form
{ initials, score }
"""

const SCORES_PATH = "user://scores"
const SCORE_SECTION = "scores"

const DEFAULT_SCORE = {
	initials = "RMS",
	score = 1,
}

var _scores = [] setget , get_scores


func register(score):
	if _scores == []:
		self.load()
	var sorted_scores = _scores
	sorted_scores.append({initials='YOU', score=score})
	sorted_scores.sort_custom(self, 'sort_scores')
	sorted_scores.pop_back()
	_scores = sorted_scores
	save()


static func sort_scores(a, b):
	return a['score'] >= b['score']


func save():
	if _scores.size() != 10:
		return
	var file = ConfigFile.new()
	for i in range(_scores.size()):
		file.set_value(SCORE_SECTION, str(i), _scores[i])
	file.save(SCORES_PATH)


func load():
	"""
	Loads scores from the disk and stores them in the _scores variable
	"""
	if not _score_file_exists():
		_scores = _get_default_scores()
		return

	var file = ConfigFile.new()
	if file.load(SCORES_PATH) != OK:
		return
	_scores = []
	for i in range(10):
		var score = file.get_value(SCORE_SECTION, str(i), DEFAULT_SCORE)
		_scores.append(score)


func get_scores():
	return _scores if _scores != [] else _get_default_scores()


func _score_file_exists():
	return Directory.new().file_exists(SCORES_PATH)


func _get_default_scores():
	var scores = []
	for i in range(10):
		scores.append(DEFAULT_SCORE)
	return scores

extends Control

onready var scores_column = $Column/ScoresColumn

const ROW_COUNT = 3

const ScoreRow = preload("ScoreRow.tscn")

var scores = []


func start(scores):
	"""Builds a list of ScoreRows and plays their start animation"""
	for i in range(ROW_COUNT):
		var row = ScoreRow.instance()
		scores_column.add_child(row)
		row.setup(i, scores[i])
	visible = true
	for row in scores_column.get_children():
		row.start()
		yield(get_tree().create_timer(0.07), "timeout")

Flossy Gnu
==========

[project] | [code] | [tracker]

A simple 2D game demonstrating adaptive game design with the Godot
game engine.

Run from source
---------------

```
git clone https://source.puri.sm/Purism/flossy-gnu.git
TODO...
```

Build deb/flatpak
-----------------

TODO...

## License and Copyright

Copyright © 2019 Purism SPC

This work is licensed under the Creative Commons
Attribution-ShareAlike 4.0 International License. To view a copy of
this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or
send a letter to Creative Commons, PO Box 1866, Mountain View, CA
94042, USA.

SPDX-License-Identifier: CC-BY-SA-4.0

Source code is under GPLv3 or later.

We adhere to the Community Covenant 1.0 without modification, and
certify origin per DCO 1.1 with a signed-off-by line. Contributions
under the same terms are welcome.

For details see:

* [COPYING.md], license notices
* [COPYING.GPL.md], full license text
* [CODE_OF_CONDUCT.md], full conduct text
* [CONTRIBUTING.DCO.md], full origin text

<!-- * [CONTRIBUTING.md], additional contribution notes -->

<!-- Links -->

[project]: https://source.puri.sm/Purism/flossy-gnu
[code]: https://source.puri.sm/Purism/flossy-gnu/tree/master
[tracker]: https://source.puri.sm/Purism/flossy-gnu/issues
[COPYING.md]: COPYING.md
[COPYING.GPL.md]: COPYING.GPL.md
[CODE_OF_CONDUCT.md]: CODE_OF_CONDUCT.md
[CONTRIBUTING.DCO.md]: CONTRIBUTING.DCO.md
